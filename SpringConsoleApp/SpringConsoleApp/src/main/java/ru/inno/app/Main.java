package ru.inno.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.inno.config.AppSpringConfig;
import ru.inno.services.UsersService;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppSpringConfig.class);
        UsersService usersService = context.getBean(UsersService.class);
        System.out.println(usersService.getAllUsers());
    }
}
