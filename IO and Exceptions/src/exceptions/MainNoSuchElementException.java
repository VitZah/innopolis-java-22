package exceptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainNoSuchElementException {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("Hello!");

        Iterator<String> iterator = strings.iterator();
        System.out.println(iterator.next());
        System.out.println(iterator.next());
    }
}
