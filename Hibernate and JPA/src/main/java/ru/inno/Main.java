package ru.inno;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ru.inno.models.Course;
import ru.inno.models.Lesson;
import ru.inno.models.Student;
import ru.inno.repository.*;

import java.time.LocalDate;
import java.time.LocalTime;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        EntityManager entityManager = sessionFactory.createEntityManager();

        CoursesRepository coursesRepository = new CoursesRepositoryJpaImpl(entityManager);
        StudentsRepository studentsRepository = new StudentsRepositoryImpl(entityManager);
        LessonsRepository lessonsRepository = new LessonsRepositoryImpl(entityManager);

//        Course java = Course.builder()
//                .description("Курс по Java")
//                .title("Java")
//                .start(LocalDate.now())
//                .finish(LocalDate.now().plusMonths(6))
//                .build();
//
//        Course php = Course.builder()
//                .description("Курс по PHP")
//                .title("PHP")
//                .start(LocalDate.now().minusMonths(1))
//                .finish(LocalDate.now().plusMonths(3))
//                .build();

//        coursesRepository.save(java);
//        coursesRepository.save(php);

        Course course = coursesRepository.findById(2L);

        System.out.println(course);

//        int i = 0;
//        Student student = studentsRepository.findById(8L);
//
//        Lesson lesson = Lesson.builder()
//                .course(course)
//                .startTime(LocalTime.of(16, 0))
//                .finishTime(LocalTime.of(17,0))
//                .summary("Создание сайтов")
//                .name("PHP - 1")
//                .build();
//
//        lessonsRepository.save(lesson);
//
//        student.getCourses().add(course);
//        student.setAge(33);
////        course.getStudents().add(student);
//        studentsRepository.save(student);

        entityManager.close();
    }
}