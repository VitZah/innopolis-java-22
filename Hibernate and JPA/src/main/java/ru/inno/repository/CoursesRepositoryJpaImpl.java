package ru.inno.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import ru.inno.models.Course;

import java.util.List;

@RequiredArgsConstructor
public class CoursesRepositoryJpaImpl implements CoursesRepository {

    private final EntityManager entityManager;

    @Override
    public void save(Course entity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(entity);
        transaction.commit();
    }

    @Override
    public List<Course> findAll() {
        return entityManager.createQuery("select course from Course course", Course.class).getResultList();
    }

    @Override
    public Course findById(Long id) {
        return entityManager.find(Course.class, id);
    }
}
