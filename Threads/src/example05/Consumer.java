package example05;

public class Consumer extends Thread {
    private final Product product;

    public Consumer(Product product) {
        super("Consumer");
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (product) {
                System.out.println("Consumer: Проверяет готовность продукта");

                while (!product.isProduced()) {
                    System.out.println("Consumer: Продукт не готов, уходим в ожидание");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                System.out.println("Consumer: Используем продукт");
                product.consume();
                System.out.println("Consumer: Оповещаем Producer-а");
                product.notify();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
