package example02;

import java.util.Scanner;
import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PrimesNumberGenerator generator = new PrimesNumberGenerator();

        while (true) {
            int from = scanner.nextInt();
            int to = scanner.nextInt();
            generator.generateNumbers("generated//" + UUID.randomUUID() + ".txt", from, to);
        }
    }
}
