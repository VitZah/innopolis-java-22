package example06;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        // создание 5 разных задач
        Runnable task1 = () -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " task 1");
            }
        };
        Runnable task2 = () -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " task 2");
            }
        };
        Runnable task3 = () -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " task 3");
            }
        };
        Runnable task4 = () -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " task 4");
            }
        };
        Runnable task5 = () -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " task 5");
            }
        };
        // создание Thread Pool из 2-х потоков и загрузка созданных задач
        try (ExecutorService service = Executors.newFixedThreadPool(2)) {
            service.submit(task1);
            service.submit(task2);
            service.submit(task3);
            service.submit(task4);
            service.submit(task5);
        }
    }
}
