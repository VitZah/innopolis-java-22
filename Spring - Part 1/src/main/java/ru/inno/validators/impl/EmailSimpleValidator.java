package ru.inno.validators.impl;

import ru.inno.validators.EmailValidator;

public class EmailSimpleValidator implements EmailValidator {

    private final String characters;

    public EmailSimpleValidator(String characters) {
        this.characters = characters;
    }

    @Override
    public void validate(String email) {
        for (char character : characters.toCharArray()) {
            if (email.indexOf(character) == -1) {
                throw new IllegalArgumentException("Incorrect email");
            }
        }
    }
}
