package ru.inno.repositories.impl;

import ru.inno.models.User;
import ru.inno.repositories.UsersRepository;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(User user) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String userToSave = user.getEmail() + "|" + user.getPassword();
            bufferedWriter.write(userToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
