package ru.inno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.inno.services.UsersServiceImpl;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

//        EmailSimpleValidator emailSimpleValidator = (EmailSimpleValidator) context.getBean("emailSimpleValidator");
//        EmailSimpleValidator emailSimpleValidator1 = context.getBean(EmailSimpleValidator.class);
//        EmailValidator emailValidator = context.getBean(EmailValidator.class);

        UsersServiceImpl usersService = context.getBean("usersService", UsersServiceImpl.class);

        usersService.signUp("marsel@marsel.com", "qwertyu199");
    }
}
