package example01;

public class Human {
    // fields - поля, конкретные значения полей - определяют состояние объекта
    double height;
    boolean isWorker;

    // конструктор с параметрами
    Human(double height, boolean isWorker) {
        this.height = height;
        this.isWorker = isWorker;
    }

    // methods - функции и процедуры внутри класса
    void grow(double value) {
        this.height += value;
    }

    void relax() {
        this.isWorker = false;
    }

    void work() {
        this.isWorker = true;
    }
}
