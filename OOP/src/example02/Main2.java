package example02;


import java.util.Arrays;
import java.util.Random;

// есть последовательность чисел (1000 штук)
// каждое число 0-99
// нужно понять, какое встречается чаще других

// O(n) + O(n) = O(n)
public class Main2 {
    public static void main(String[] args) {
        Random random = new Random();

        int[] numbers = new int[1000];

        // создал 1000 случайных чисел
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = random.nextInt(100);
        }

        System.out.println(Arrays.toString(numbers));

        int[] counts = new int[100];

        for (int i = 0; i < numbers.length; i++) {
            // если попадется число 77
            // counts[77]++; // под индексом 77 будет лежать количество раз, сколько встретилось число 77
            counts[numbers[i]]++;
        }

        // потом найти максимум в массиве counts
        for (int i = 0; i < counts.length; i++) {
            System.out.println(i + " - " + counts[i]);
        }

    }
}
