drop table if exists student_course;
drop table if exists student;
drop table lesson;
drop table if exists course;

create table student
(
    id         bigserial primary key, -- первичный ключ (большое целое), генерируется СУБД
    email      varchar(20) unique, -- уникальное строковое значение с максимальной длиной 20
    password   varchar(100),
    first_name varchar(20) default 'DEFAULT_FIRSTNAME', -- строковое значение со значением по умолчанию
    last_name  varchar(20) default 'DEFAULT_LASTNAME',
    age        integer check (age >= 0 and age <= 120) not null, -- числовое значение, с проверкой значения, не допускается null
    is_worker  bool -- логическое значение
);

alter table student
    alter column email set not null;
alter table student
    add column average double precision check ( average >= 0 and average <= 5) default 0;

create table course
(
    id          serial primary key,
    title       char(10) not null,
    description char(500),
    start       timestamp,
    finish      timestamp
);

create table lesson
(
    id          serial primary key,
    name        char(20),
    summary     char(1000),
    start_time  time,
    finish_time time,
    course_id   integer not null,
    foreign key (course_id) references course (id) -- внешний ключ, course_id ссылается на id таблицы course
);

create table student_course (
                                student_id bigint,
                                course_id bigint,
                                foreign key (student_id) references student(id),
                                foreign key (course_id) references course(id)
);

alter table lesson
    alter column course_id drop not null