package lambda;

public class OnlyEvenNumbersProcessor implements NumbersProcessor {

    @Override
    public int process(int number) {
        // если число четное
        if (number % 2 == 0) {
            // возвращаем это число
            return number;
        } else {
            // если оно не четное, возвращаем 0
            return 0;
        }
    }
}
