package lambda;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        NumbersProcessor onlyEven = new OnlyEvenNumbersProcessor();
        NumbersProcessor onlyOdd = new OnlyOddNumbersProcessor();

        NumbersProcessor only7Ends = new NumbersProcessor() {
            @Override
            public int process(int number) {
                if (number % 10 == 7) {
                    return number;
                } else {
                    return 0;
                }
            }
        };

        /* f(x) = x^2
            x -> {
                return x * x;
            }
        */
        NumbersProcessor only6Ends = x -> {
            if (x % 10 == 6) {
                return x;
            } else {
                return 0;
            }
        };

        ArrayNumbersConverter converter = new ArrayNumbersConverter();
        int[] array = {10, 4, 15, 66, 77, 11, 22};

        int[] newArray = converter.convert(array, x -> x * x);

        System.out.println(Arrays.toString(newArray));

        TwoOperandsFunction function = (a, b) -> a + b;

        System.out.println(function.apply(10, 15));

    }
}
