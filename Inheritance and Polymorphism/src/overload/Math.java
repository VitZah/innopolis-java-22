package overload;

public class Math {
    // overload - перегрузка методов
    //
    public int sum(int a, int b) {
        return a + b;
    }

    public int sum(int a, int b, int c) {
        return  a + b + c;
    }

    // если вы поменяете тип возвращаемого значения - это не будет допустимо
//    public double sum(int a, int b) {
//        return a + b;
//    }
}
