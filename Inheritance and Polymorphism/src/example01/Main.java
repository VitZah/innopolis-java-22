package example01;

public class Main {
    public static void main(String[] args) {
        Human human = new Human("Марсель", "Сидиков");
        human.setAge(-10);

        Programmer programmer =
                new Programmer("Виктор", "Евлампьев",
                        "PHP");

        programmer.setAge(26);
        programmer.setExperience(13);

        human.go();
        programmer.go();

    }
}
