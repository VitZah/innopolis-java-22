package example05;

public class Worker extends Human implements Professional, Runner {
    public Worker(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void go() {
        System.out.println("Я хожу только на работу");
    }

    @Override
    public void tellAbout() {
        System.out.println("Я строитель!");
    }

    public void build() {
        System.out.println("Строю дом");
    }


    @Override
    public void giveMoney(int sum) {
        System.out.println("Ура, получил зарплату, надо отдать жене " + sum + "рублей");
    }

    @Override
    public void goVacation() {
        System.out.println("Жена, поехали в отпуск!");
    }

    @Override
    public void runFromWork() {
        System.out.println("Да, дорогая, сейчас буду дома!");
    }
}
