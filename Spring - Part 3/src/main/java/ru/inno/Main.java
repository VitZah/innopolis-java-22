package ru.inno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.inno.config.AppConfig;
import ru.inno.services.UsersServiceImpl;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        UsersServiceImpl usersService = context.getBean(UsersServiceImpl.class);

        usersService.signUp("marsel@marsel.com", "qwertyu199");
    }
}
