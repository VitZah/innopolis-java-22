package ru.inno.validators.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.inno.validators.EmailValidator;

@Component
public class EmailSimpleValidator implements EmailValidator {

    @Value("${email.simple.validator.characters}")
    private String characters;

    @Override
    public void validate(String email) {
        for (char character : characters.toCharArray()) {
            if (email.indexOf(character) == -1) {
                throw new IllegalArgumentException("Incorrect email");
            }
        }
    }
}
