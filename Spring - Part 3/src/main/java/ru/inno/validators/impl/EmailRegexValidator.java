package ru.inno.validators.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.inno.validators.EmailValidator;

import java.util.regex.Pattern;

@Component
public class EmailRegexValidator implements EmailValidator {

    @Value("${email.regex.validator.regex}")
    private String regex;

    @Override
    public void validate(String email) {
        if (!Pattern.compile(regex).matcher(email).matches()) {
            throw new IllegalArgumentException("Incorrect email format");
        }
    }
}
