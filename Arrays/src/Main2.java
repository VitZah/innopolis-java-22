import java.util.Scanner;

// поиск числа в произвольном массиве
public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();
        // явная инициализация массива
        int[] array = {10, 13, 11, 12, 2, 4};

        // exists = true, если число в массиве есть
        // exists = false, если числа в массиве нет
        boolean exists = false;

        for (int i = 0; i < array.length; i++) {
            // если мы нашли нужное число
            if (array[i] == number) {
                // делаем флаг - true
                exists = true;
                // останавливаем цикл
                break;
            }

        }
        // exists = true, false
        if (exists) {
            System.out.println("Число в массиве есть!");
        } else {
            System.out.println("Числа в массиве нет!");
        }
    }
}
