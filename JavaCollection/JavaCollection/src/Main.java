import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Marsel");
        list.add("Sidikov");
        list.add("28");

//        Iterator<String> iterator = list.iterator();
//
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
//        }

        // foreach
        for (String s : list) {
            System.out.println(s);
        }

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));

        list.remove("Sidikov");
        list.remove(0);

        System.out.println(list);
    }
}