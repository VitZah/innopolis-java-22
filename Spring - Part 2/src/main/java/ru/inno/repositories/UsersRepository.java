package ru.inno.repositories;

import ru.inno.models.User;

public interface UsersRepository {
    void save(User user);
}
