package ru.inno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.inno.services.UsersServiceImpl;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        UsersServiceImpl usersService = context.getBean(UsersServiceImpl.class);

        usersService.signUp("marsel@marsel.com", "qwertyu199");
    }
}
