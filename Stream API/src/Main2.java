import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main2 {
    public static void main(String[] args) {
        List.of("Hello", "Bye", "how", "are", "You").
                stream()
                .filter(word -> Character.isUpperCase(word.toCharArray()[0]))
                .map(word -> word.toUpperCase())
                .forEach(word -> System.err.println(word));
    }
}