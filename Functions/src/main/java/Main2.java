import java.lang.reflect.Array;
import java.util.Arrays;

public class Main2 {


    // сортировка меняет исходный массив
    public static void sort(int[] array) {
        int min, indexOfMin, temp;
        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;

            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }

            temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }

    // обнуляем элементы исходного массива
    public static void makePositive(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0) {
                array[i] = 0;
            }
        }
    }

    public static void swap(int x, int y) {
        int temp = x;
        x = y;
        y = temp;
    }

    public static void main(String[] args) {
        int a[] = {30, 10, 15, -1, -10, 100, -5};

        sort(a);
        System.out.println(Arrays.toString(a));
        makePositive(a);

        System.out.println(Arrays.toString(a));

        int c = 10;
        int d = 15;

        int temp = c;
        c = d;
        d = temp;

        System.out.println(Math.sqrt(16));

//        swap(c, d);

        System.out.println("c - " + c + ", d - " + d);
    }
}
