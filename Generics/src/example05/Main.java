package example05;

public class Main {
    public static void main(String[] args) {
        Container<Integer> container = new Container<>();
        container.add(4);
        container.add(7);
        container.add(8);

        int value = container.get(1);
        System.out.println(value); // 7

        Container<String> container1 = new Container<>();
        container1.add("Привет");
        container1.add("Как дела");
        container1.add("Bye");

        String value1 = container1.get(1);
        System.out.println(value1); // Как дела

//        NumbersContainer<Number>
    }
}
