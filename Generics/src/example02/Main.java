package example02;

public class Main {
    public static void main(String[] args) {
        Nokia nokia = new Nokia();
        iPhone iPhone = new iPhone();

        Cover coverForIPhone = new Cover(nokia);
        Cover coverForNokia = new Cover(iPhone);

        Object nokiaFromCoverAsObject = coverForNokia.getPhone();
        Object iPhoneFromCoverAsObject = coverForIPhone.getPhone();

        Nokia nokiaFromCover = (Nokia)nokiaFromCoverAsObject;
        example02.iPhone iPhoneFromCover = (example02.iPhone)iPhoneFromCoverAsObject;


        nokiaFromCover.call();
        iPhoneFromCover.photo();
    }
}