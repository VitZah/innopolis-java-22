package ru.inno.education.repositories;

import ru.inno.education.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from student order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into student(email, password, first_name, last_name, age, is_worker, average) " +
            "values (?, ?, ?, ?, ?, ?, ?)";

    private DataSource dataSource;

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Student> userRowMapper = row -> {
        try {
            return Student.builder()
                    .id(row.getLong("id"))
                    .email(row.getString("email"))
                    .password(row.getString("password"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .average(row.getDouble("average"))
                    .age(row.getInt("age"))
                    .isWorker(row.getBoolean("is_worker"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Student> findAll() {
        List<Student> students = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Student student = userRowMapper.apply(resultSet);
                    students.add(student);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

        return students;
    }

    @Override
    public void save(Student student) {
        // создаем объект PreparedStatement внутри try-with-resources, чтобы автоматически все закрыть
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            // кладем нужные параметры для текущего запроса из объекта student
            preparedStatement.setString(1, student.getEmail());
            preparedStatement.setString(2, student.getPassword());
            preparedStatement.setString(3, student.getFirstName());
            preparedStatement.setString(4, student.getLastName());
            preparedStatement.setInt(5, student.getAge());
            preparedStatement.setBoolean(6, student.getIsWorker());
            preparedStatement.setDouble(7, student.getAverage());
            // выполняем запрос и получаем количество строк, которые были изменены
            int affectedRows = preparedStatement.executeUpdate();
            // если была изменена не одна строка, то выбрасываем ошибку
            if (affectedRows != 1) {
                throw new SQLException("Can't insert student");
            }
            // получаем итератор по сгенерированным ключам в БД для текущего запроса
            try (ResultSet generatedId = preparedStatement.getGeneratedKeys()) {
                // нужно сдвинуть итератор на первую позицию
                if (generatedId.next()) {
                    // забрать сгенерированный id и отправить его в студента
                    student.setId(generatedId.getLong("id"));
                } else {
                    // если сдвинуть итератор не получилось, выбрасываем исключение
                    throw new SQLException("Can't obtain generated id");
                }
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
